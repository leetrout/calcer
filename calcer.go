// Sample code to parse CSV input on stdin and output calculated results
// on stdout.
//
// Example execution and output with csvlook:
//
// go run calcer.go < input.csv | csvlook 2> /dev/null
// | Rider ID                | Request to Pickup | Pickup to Drop-Off | Request to Drop-Off | e       | f       | g       |
// | ----------------------- | ----------------- | ------------------ | ------------------- | ------- | ------- | ------- |
// | R35                     | 1m45s             | 41m10s             | 42m55s              |         |         |         |
// | R23                     | 1m59s             | 34m0s              | 35m59s              |         |         |         |
// | R66                     | 10m48s            | 29m54s             | 40m42s              |         |         |         |
// | R71                     | 12m38s            | 46m30s             | 59m8s               |         |         |         |
// | R38                     | 13m35s            | 6m25s              | 20m0s               |         |         |         |
// | R45                     | 7m16s             | 7m50s              | 15m6s               |         |         |         |
// |                         |                   |                    |                     |         |         |         |
// | System Wide             | Event 1           | Event 2            | Event 3             | Event 4 | Event 5 | Event 6 |
// | Elapsed Time: Requests  | 0                 | 3m18s              | 14m2s               | 26m11s  | 14m8s   | 33m14s  |
// | Elapsed Time: Pickups   | 0                 | 3m32s              | 22m51s              | 28m1s   | 15m5s   | 26m55s  |
// | Elapsed Time: Drop-Offs | 0                 | 3m38s              | 15m7s               | 19m37s  | 25m0s   | 3m20s   |
//
package main

import (
	"encoding/csv"
	"fmt"
	"os"
	"time"
)

// Time layout to parse our input times.
// Using Go's "magic numbers":
// https://golang.org/pkg/time/#pkg-constants
const timeLayout = "2006-01-02 15:04:05"

// CSV input indicies
const (
	csvIdxTime = iota
	csvIdxRiderID
	csvIdxDriverID
	csvIdxEvent
)

// CSV output indicies and total count
const (
	statsIdxRiderID = iota
	statsIdxReqToPickup
	statsIdxPickupToDrop
	statsIdxReqToDrop
	statsColumnCount = 7 // Matches the width of the full stats output
)

// RiderID represents a rider
type RiderID string

// Event represents an event
// Request, Pickup, Drop-Off
type Event string

const (
	EventRequest Event = "Request"
	EventPickUp  Event = "Pickup"
	EventDropOff Event = "Drop-Off"
)

// RequestMap maps rider IDs to their request time
type RequestMap map[RiderID]time.Time

// PickUpMap maps rider IDs to their pickup time
type PickUpMap map[RiderID]time.Time

// DropOffMap maps rider IDs to their dropoff time
type DropOffMap map[RiderID]time.Time

// RequestTimes is a slice of request times
type RequestTimes []time.Time

// PickUpTimes is a slice of pickup times
type PickUpTimes []time.Time

// DropOffTimes is a slice of drop off times
type DropOffTimes []time.Time

// Helper types for ordered output.
type empty struct{}
type riderIDSet map[RiderID]empty

// Bucket is our collection of time maps
type Bucket struct {
	// Map requests by rider ID
	Requests RequestMap
	PickUps  PickUpMap
	DropOffs DropOffMap

	// Keep ordered lists of event times
	RequestTimes RequestTimes
	PickUpTimes  PickUpTimes
	DropOffTimes DropOffTimes

	// Unexported fields to help walk through riders in order
	orderedRiders []RiderID
	allRiders     map[RiderID]empty
}

// Fill runs through the given CSV rows and adds times
// to the appropriate bucket based on the event.
func (b *Bucket) Fill(csvRows [][]string) {
	for _, row := range csvRows {
		riderID := RiderID(row[csvIdxRiderID])

		when := parseTime(row[csvIdxTime])
		switch Event(row[csvIdxEvent]) {
		case EventRequest:
			b.Requests[riderID] = when
			b.RequestTimes = append(b.RequestTimes, when)
		case EventPickUp:
			b.PickUps[riderID] = when
			b.PickUpTimes = append(b.PickUpTimes, when)
		case EventDropOff:
			b.DropOffs[riderID] = when
			b.DropOffTimes = append(b.DropOffTimes, when)
		default:
			// TODO Default with error or warning for unknown event in "real code"
		}

		// Keep track of the order we read riders in from this
		// csv so we write it back out in the same order.
		// This is relying on the input being ordered correctly
		// and a request showing up the first time we see a rider
		if _, ok := b.allRiders[riderID]; !ok {
			b.orderedRiders = append(b.orderedRiders, riderID)
			b.allRiders[riderID] = empty{}
		}
	}
}

// generateStatsHeader creates the general stats header
func (b *Bucket) generateStatsHeader() []string {
	header := make([]string, statsColumnCount)
	header[statsIdxRiderID] = "Rider ID"
	header[statsIdxPickupToDrop] = "Pickup to Drop-Off"
	header[statsIdxReqToDrop] = "Request to Drop-Off"
	header[statsIdxReqToPickup] = "Request to Pickup"
	return header
}

// generateRiderRows creates a row of stats for each rider
func (b *Bucket) generateRiderRows() [][]string {
	output := [][]string{}
	for _, riderID := range b.orderedRiders {
		// Use make and indicies to rely on iota for columns
		newRow := make([]string, statsColumnCount)
		newRow[statsIdxRiderID] = string(riderID)
		newRow[statsIdxPickupToDrop] = formatDuration(b.DropOffs[riderID].Sub(b.PickUps[riderID]))
		newRow[statsIdxReqToDrop] = formatDuration(b.DropOffs[riderID].Sub(b.Requests[riderID]))
		newRow[statsIdxReqToPickup] = formatDuration(b.PickUps[riderID].Sub(b.Requests[riderID]))
		output = append(output, newRow)
	}
	return output
}

// generateSystemStats generates elapsed time stats between events
func (b *Bucket) generateSystemStats() [][]string {
	output := [][]string{}

	totalRequests := len(b.RequestTimes)
	// The table of system stats is one column wider since the
	// first column contains labels
	statsWidth := totalRequests + 1

	// Allocate rows for system stats
	statsHeader := make([]string, statsWidth)
	elapsedRequests := make([]string, statsWidth)
	elapsedPickUps := make([]string, statsWidth)
	elapsedDropOffs := make([]string, statsWidth)

	// Set the first column in all these rows
	statsHeader[0] = "System Wide"
	elapsedRequests[0] = "Elapsed Time: Requests"
	elapsedPickUps[0] = "Elapsed Time: Pickups"
	elapsedDropOffs[0] = "Elapsed Time: Drop-Offs"

	// Assumes all time slices are equal with totalRequests
	for i := 0; i < totalRequests; i++ {
		j := i + 1
		statsHeader[j] = fmt.Sprintf("Event %d", i+1)
		// On the first loop everything is going to be
		// zero because there isn't a previous event to compare
		if i == 0 {
			elapsedRequests[j] = "0"
			elapsedPickUps[j] = "0"
			elapsedDropOffs[j] = "0"
			continue
		}

		// Subtract the previous event time (i-1) from the current time (i)
		elapsedRequests[j] = formatDuration(b.RequestTimes[i].Sub(b.RequestTimes[i-1]))
		elapsedPickUps[j] = formatDuration(b.PickUpTimes[i].Sub(b.PickUpTimes[i-1]))
		elapsedDropOffs[j] = formatDuration(b.DropOffTimes[i].Sub(b.DropOffTimes[i-1]))
	}

	output = append(output, statsHeader)
	output = append(output, elapsedRequests)
	output = append(output, elapsedPickUps)
	output = append(output, elapsedDropOffs)
	return output
}

// PrintStats outputs a CSV formatted summary to stdout.
// It assumes all data is represented by request keys and
// loops off of that data set.
func (b *Bucket) PrintStats() {
	output := [][]string{}

	// Add headers
	output = append(output, b.generateStatsHeader())

	// Add each row of rider data
	output = append(output, b.generateRiderRows()...)

	// Add an empty row before our stats
	output = append(output, make([]string, statsColumnCount))

	// Add systemwide stats
	output = append(output, b.generateSystemStats()...)

	// Send it to stdout
	w := csv.NewWriter(os.Stdout)
	w.WriteAll(output)
}

// NewBucket creates a bucket with all maps instantiated
func NewBucket() *Bucket {
	return &Bucket{
		Requests:      RequestMap{},
		PickUps:       PickUpMap{},
		DropOffs:      DropOffMap{},
		RequestTimes:  RequestTimes{},
		PickUpTimes:   PickUpTimes{},
		DropOffTimes:  DropOffTimes{},
		orderedRiders: []RiderID{},
		allRiders:     riderIDSet{},
	}
}

// main handles the basic app runtime
// + Parse a CSV from standard input
// + Build internal represenation of data
// + Output calculations
func main() {
	csvRows, err := parseInputAsCSV()
	if err != nil {
		fatal(fmt.Errorf("Could not parse CSV: %w", err))
	}

	bucket := NewBucket()
	bucket.Fill(csvRows[1:]) // NB First row of input CSV is headers so slice it off
	bucket.PrintStats()
}

//
// Utility helpers
//

// parseTime parses the given time string and swallows errors
func parseTime(raw string) time.Time {
	t, _ := time.Parse(timeLayout, raw)
	return t
}

// formatDuration formats the given duration
func formatDuration(t time.Duration) string {
	return t.String()
}

// parseInputAsCSV runs stdin through CSV ReadAll
func parseInputAsCSV() ([][]string, error) {
	csvFile := csv.NewReader(os.Stdin)
	return csvFile.ReadAll()
}

// fatal is a simple helper to print an error and exit 1
func fatal(e error) {
	fmt.Println(e)
	os.Exit(1)
}
